const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, '../output'),
    filename: "main.js"
  },
  module: {
    rules: [
      {
        test: /\.jsx|js$/,
        exclude: /node_modules/,
        use: [
            {
                loader: 'babel-loader',
                query: {
                    presets: [
                      '@babel/preset-react',
                      [
                        '@babel/preset-env', { 
                        modules: 'commonjs',
                        "targets": {
                          "esmodules": true
                          } 
                        }
                      ], 
                      {
                        plugins: [
                          'transform-es2015-destructuring','transform-es2015-arrow-functions','transform-jscript','transform-object-set-prototype-of-to-assign', '@babel/plugin-proposal-class-properties'
                        ]
                      }
                    ],
                },
            }
        ]
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.html/,
        use: 'html-loader',
      },
      {
        test: /\.less|.css$/,
        loader: ["style-loader", "css-loader", 'less-loader']
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: 'src/assets/index.html',
      }),  
  ],
};