import React from 'react';
import block from 'bem-cn';

import './MatchedGroup.less';

export function GroupMatchedItems(props){
    const { title, id, children, style={}, customClassName } = props;
    const className = block('category');
    return(
        <div style={style} key={id} className={customClassName || className()}>
            <span>{title}</span>
            {children}
        </div>
    )
}