import React from 'react';
import block from 'bem-cn';

import { splitStringByIndexes } from '../../../utils/utils';
import  SearchIcon  from './img/search.svg';
import './Input.less'

const inputKinds = {
    cutDown: 'cut-down',
    default: '',
}

function getInputKind(kind){
    return inputKinds[kind] || inputKinds.default;
}


function getSelectText(selectPositions, value, className) {
    const { from, to } = selectPositions;
    const [ textBeforeSelected, selectedText, textAfterSelected ] = splitStringByIndexes(value, from, to);
    return(
            <>
                {textBeforeSelected}
                <span className={className('selected-text')}>
                    {selectedText}
                </span>
                {textAfterSelected}
            </>)
}

export function Input(props){
    const { value, selectPositions = null, inputKind } = props;
    const className = block('input');
    const selectedString = selectPositions ? getSelectText(selectPositions, value, className): value;
    const modificatorKind = getInputKind(inputKind);
    return(
        <div className={className({ [modificatorKind]: !!modificatorKind })}>
            <div className={className('text-select-wrapper')}>
                {selectedString}
            </div>
            <input className={className('input')} {...props}/>
        </div>
    )
}