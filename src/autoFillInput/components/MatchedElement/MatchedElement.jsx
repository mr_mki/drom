import React from 'react';
import block from 'bem-cn';

import './MatchedElement.less';

export function MatchedElement(props) {
    const { onMouseOver, indexOfOccurrence, showValue, value, style={}, customClassName, onClick, id } = props;
    const className = block('matched-element');
    return(
        <div 
            onMouseOver={() => onMouseOver(showValue, indexOfOccurrence, value)} 
            key={id}
            style={style}
            className={customClassName || className()}
            onMouseDown={() => onClick(showValue)}
        >
            {showValue}
        </div>
    )
}
