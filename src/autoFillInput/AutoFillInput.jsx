import React, { Component } from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';

import { AutoFillManager } from '../../AutoFillManager';
import { MatchedElement } from './components/MatchedElement';
import { Input } from './components/Input';

export class AutoFillInput extends Component {
    static propTypes = {
        dataSource: PropTypes.oneOfType([
            PropTypes.func,
            PropTypes.shape({
                url: PropTypes.string,
                method: PropTypes.string,
                arguments: PropTypes.arrayOf(PropTypes.object)
        })]),

        getValue: PropTypes.func,      

        filterRule: PropTypes.func,

        onDataSetMustUpdate: PropTypes.func,
        groupsElements: PropTypes.shape({
            getGroup: PropTypes.func,
            renderGroupWrapper: PropTypes.func,
        }),
        renderElements: PropTypes.func,
    }

    constructor(props){
        super(props);
        this.state = {
            inputValue: '',
            selectedValue:'',
            selectPositions: '',
        }
    }

    componentDidMount() {
        const { dataSource, getValue, onDataSetMustUpdate, groupsElements={} } = this.props;
        const { getGroup } = groupsElements;
        this.AutoFillManager = new AutoFillManager(dataSource, getValue, onDataSetMustUpdate, getGroup);
    }

    getElements = () => {
        const { inputValue } = this.state;
        const elements = this.AutoFillManager.getElements(inputValue);
        this.setState({ elements });
    } 

    onInputChange = (e) => {
        const inputValue = e.target.value;
        this.setState(
            { inputValue },
            this.getElements,
        );
        
    }

    onMouseOverOnMatchedElement = (showValue, indexOfOccurrence, value) => {
        this.setState({ 
            selectedValue: showValue,
            selectPositions: {
                from: indexOfOccurrence,
                to: value.length,
            }
        });
    }

    onMouseLeaveOnMatchedElement = () => {
        this.setState({ 
            selectedValue: '',
            selectPositions: null,
        });
    }

    render() {
        const { inputValue, elements=[], selectedValue, selectPositions } = this.state;
        const className = block('auto-fill-input')
        const matchedValues = elements.map( 
            el => <MatchedElement 
                        onMouseOver = {this.onMouseOverOnMatchedElement}
                        indexOfOccurrence={el.indexOfOccurrence} 
                        showValue={el.showValue} 
                        value={el.inputValue}
                    />
        );

        return (<div className={className}>
            <Input value={ selectedValue || inputValue} selectPositions={selectPositions} onChange={this.onInputChange} />
            <div  
                className={className('items-list-wrapper')} 
                onMouseLeave = {this.onMouseLeaveOnMatchedElement}
            >
                {matchedValues}
            </div>
        </div>)
    }

}
