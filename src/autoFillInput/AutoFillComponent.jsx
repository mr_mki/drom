import React, { Component } from 'react';
import PropTypes from 'prop-types';
import block from 'bem-cn';

import { AutoFillManager } from '../autoFillManager';
import { MatchedElement } from './components/MatchedElement';
import { GroupMatchedItems } from './components/MatchedGroup';
import { Input } from './components/Input';

import './AutoFillComponent.less';

export class AutoFillComponent extends Component {
    static propTypes = {
        dataSource: PropTypes.oneOfType([
            PropTypes.func,
            PropTypes.shape({
                url: PropTypes.string,
                method: PropTypes.string,
                arguments: PropTypes.arrayOf(PropTypes.object)
        })]),

        getValue: PropTypes.func,      

        filterRule: PropTypes.func,

        onDataSetMustUpdate: PropTypes.func,
        getCategory: PropTypes.func,
        renderGroupWrapper: PropTypes.func,
        renderElements: PropTypes.func,

        onChange: PropTypes.function,
    }

    constructor(props){
        super(props);
        this.state = {
            inputValue: '',
            selectedValue:'',
            selectPositions: '',
        }
    }

    componentDidMount() {
        const { dataSource, getValue, onDataSetMustUpdate, getCategory, filterRule } = this.props;
        this.autoFillManager = new AutoFillManager(dataSource, getValue, onDataSetMustUpdate, getCategory, filterRule);
    }

    componentDidUpdate() {
        const { elements, selectedValue } = this.state;
        const isElementsListFirstlyEmpty = !(elements && elements.length) && selectedValue
        if(isElementsListFirstlyEmpty) {
            this.resetSelectedItem();
        }
    }

    getElements = () => {
        const { inputValue } = this.state;
        const elements = this.autoFillManager.getElements(inputValue);
        this.setState({ elements });
    } 

    onInputChange = (e) => {
        const { onValueChange } = this.props;
        const inputValue = e.target.value;
        this.setState(
            { inputValue },
            this.getElements,
        );
        onValueChange && onValueChange(inputValue)
    }

    onInputFocus = () => {
        return this.setState({ inputInFocus: true });
    }

    onInputBlur = () => this.setState({ inputInFocus: false });

    onMouseOverOnMatchedElement = (showValue, indexOfOccurrence, value) => {
        this.setState({ 
            selectedValue: showValue,
            selectPositions: {
                from: indexOfOccurrence,
                to: value.length,
            }
        });
    }

    onMouseLeaveOnMatchedElement = () => {
        this.resetSelectedItem();
    }
    resetSelectedItem = () => {
        this.setState({ 
            selectedValue: '',
            selectPositions: null,
        });
    }

    getMatchedElements = (matchedElements) => {
        const { CustomElementWrapper, elementClassName, elementStyle = {}, selectedValue, itemProps={} } = this.props;
        const ActualElementWrapper = CustomElementWrapper 
            ? CustomElementWrapper 
            : MatchedElement;

        const matchedElementsComponents = matchedElements.map(
            el => {
            const isActive = selectedValue === el.showValue;
            return  (<ActualElementWrapper 
                        onMouseOver = {this.onMouseOverOnMatchedElement}
                        indexOfOccurrence={el.indexOfOccurrence} 
                        showValue={el.showValue} 
                        value={el.inputValue}
                        style={elementStyle}
                        id={el.id}
                        customClassName={elementClassName}
                        isActive={isActive}
                        onClick={this.onItemClick}
                        {...itemProps}
                />)
        });
        return matchedElementsComponents;
    }

    getMatchedCategories = (elements) => {
        const { CustomGroupWrapper, groupWrapperClassName, groupWrapperStyle={}, categoryProps={}  } = this.props;
        const ActualGroupWrapper = CustomGroupWrapper 
            ? CustomGroupWrapper 
            : GroupMatchedItems;

        const matchedCategories = elements.map(
            el => {
            const { elements: matchedElements } = el;
            const matchedElementsComponents = this.getMatchedElements(matchedElements);

            return (
                <ActualGroupWrapper 
                    title={el.title}
                    key={el.id}
                    style={groupWrapperStyle}
                    customClassName={groupWrapperClassName}
                    {...categoryProps}
                >
                    {matchedElementsComponents}
                </ActualGroupWrapper>)
            }
                
        )
        return matchedCategories;
    }

    onSearch = () => {
        const { inputValue } = this.state;
        const { onSearch } = this.props;
        onSearch(inputValue);
    }

    onItemClick = (showValue) => {
        this.setState({ inputValue: showValue });
    }

    render() {
        const { style={}, className: customClassName } = this.props;
        const { inputValue, elements=[], selectedValue, selectPositions, inputInFocus } = this.state;
        const className = block('auto-fill-input')
        const matchedCategories = this.getMatchedCategories(elements);

        const isItemListDisplay = matchedCategories.length && inputInFocus;
        const classNameItemList = customClassName || className('items-list-wrapper', { hidden: !isItemListDisplay });
        const inputKind = isItemListDisplay ? 'cutDown': '';

        return (<div className={className()} onBlur = {this.onInputBlur}>
            <Input 
                value={ selectedValue || inputValue} 
                selectPositions={selectPositions} 
                inputKind={inputKind}
                onChange={this.onInputChange} 
                onFocus = {this.onInputFocus}
            />
            <div  
                className={classNameItemList} 
                onMouseLeave = {this.onMouseLeaveOnMatchedElement}
                style={style}
            >
                {matchedCategories}
            </div>
        </div>)
    }

}
