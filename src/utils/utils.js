export function splitStringByIndexes(string, from, to) {
    if(!string) return 
    const textBeforSelected = string.substring(0, from);
    const textAfterSelected = string.substring(to);
    const selectedText = string.substring(from, to);
    return [textBeforSelected, selectedText, textAfterSelected];
}
