function getUrlWithParams (url, params={}) {
    const paramsKeys = Object.keys(params);
    if(!paramsKeys) return url

    const fullUrl = paramsKeys.reduse( 
        (accumUrl, currentParamKey, index) => {
            if(!index) return accumUrl + '?' + currentParamKey + '=' + params[currentParamKey];
            return accumUrl + '&' + currentParamKey + '=' + params[currentParamKey];
        } 
    )
    return fullUrl;
}

const methodRequestHandlers = {
    'get': (url, args) =>  fetch(getUrlWithParams(url, args)),
    'post': (url, args) => fetch(
        url, 
        { 
            method: 'POST', 
            body: JSON.stringify(args) 
        }
    ),
    'put': (url, args) => fetch(
        url, 
        { 
            method: 'PUT', 
            body: JSON.stringify(args) 
        }
    ),
    'delete': (url, args) => fetch(
        url, 
        { 
            method: 'delete', 
            body: JSON.stringify(args) 
        }
    ),
}

export async function sendQuery(url, method='get', args) {
    const validMethod = method.toLowerCase(method);
    return await methodRequestHandlers[method] 
        ? methodRequestHandlers[method](url, method, args)
        : methodRequestHandlers[method](url, 'get')
}
