import React, { Component }  from 'react';
import ReactDOM from 'react-dom';
import 'reset-css';

import { AutoFillComponent } from './autoFillInput'

function getDataSource() {
    return (new Array(60)).fill({
        value: 'test',
    }).map( (el, index)=> ({ value: el.value + index, title: index % 2, itemId: index,  id: index % 2}) )
}


ReactDOM.render(
    <div style={{marginLeft: '40px' }} >
        <AutoFillComponent
            dataSource={getDataSource}
            getValue={(x) => ({value: x.value, id: x.itemId}) }
            getCategory = { (el) => ({ title: el.title, id: el.id }) }
            filterRule = { (el, inputValue) => ({
                inputValue,
                showValue: el.value,
                indexOfOccurrence: 5,
                id: el.id
            })}

            // CustomElementWrapper={(props) => { return <div style={{color: 'green'}}>{props.showValue}</div> }}
            onDataSetMustUpdate={ (e, test) => {console.log(test);  return  e.length % 2 }}
        />
    </div>, 
    document.querySelector('#root')
);