export function getSubstringRule (el, inputValue) {
    const isValuesStrings = (typeof(el.value) === 'string') && (typeof(inputValue) === 'string');
    if(!isValuesStrings || !inputValue) return null;

    const indexOfOccurrence = el.value.indexOf(inputValue);
    const resultElement = { 
        inputValue,
        showValue: el.value,
        indexOfOccurrence,
        id: el.id
    }   
    return indexOfOccurrence !== -1 ? resultElement: null;
}
