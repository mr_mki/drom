import { sendQuery } from '../api';
import { getSubstringRule } from './rules.js'

class Category {
    constructor(dataConverter, el){
        let category = { id: null, title:'', elements: [] };
        if(dataConverter instanceof Function) {
            category = {...dataConverter(el)}
        } 
        const { id, title, elements } = category;

        this.id = id;
        this.title = title;
        this.elements = elements;
    }
}

export class AutoFillManager {
    constructor(dataSource, dataConverter, onDataSetMustUpdate, getCategory, rule = getSubstringRule) {
        this.dataSource = dataSource;
        this.dataConverter = dataConverter;
        this.onDataSetMustUpdate = onDataSetMustUpdate;
        this.getCategory = getCategory;
        this.rule = rule;

        this.dataSet = [];
    }

    getElements = (value) => {
        const isDatasetMustUpdate = this.onDataSetMustUpdate ? this.onDataSetMustUpdate(value, this.handledElements): true;
        const elements = isDatasetMustUpdate ? this.__uploadDataSet(value): this.dataSet;
        const rule = this.rule;
        const converter = this.dataConverter;
        const getCategory = this.getCategory;
        const handledElements = this.__handleElements(elements, rule, converter, getCategory, value);
        this.handledElements = handledElements;
        return handledElements ;
    }

    __uploadDataSet = (inputValue) => {
        const dataSource = this.dataSource;
        if(!dataSource) throw 'DataSource is not valid';

        if (dataSource instanceof Function) {
            return this.__getDataSetByCallBack(dataSource, inputValue)
        }

        if (dataSource instanceof Array) {
            this.dataSet = dataSource;
            return dataSource;
        }
        return getDataByApi(dataSource)
    }

    __handleElements = (elements, rule, converter, getCategoryCustom, value) => {
        const listElements = elements.reduce( 
            (resultList, el) => this.__handleElement(resultList, el, rule, converter, getCategoryCustom, value),
            []
        );
        return listElements;
    }

    __handleElement = (resultList, el, rule, converter, getCategoryCustom, value) => {
        const convertedItem = converter instanceof Function ? converter(el) : el;
        const desiredItem = rule(convertedItem, value);
        if(desiredItem) {
             const itemCategory = this.__getCategory(el, getCategoryCustom)
             const categoriesList = this.__appendToCategory(itemCategory, resultList, desiredItem);
             return categoriesList;
        }
        return resultList;
    }

    __getCategory(el, getCategoryCustom) {
        return new Category(getCategoryCustom, el)
    }
    __appendToCategory(itemCategory, categoriesList, desiredItem) {
        const isCategoriesListNotEmpty = categoriesList && categoriesList.length;
        if(!isCategoriesListNotEmpty){
            return [{...itemCategory, elements: [desiredItem] }]
        }

        const categoryIndex = categoriesList
            .findIndex( category => category.id === itemCategory.id);

        let resultCategoryList = null; // 

        const isNotNewCategory = categoryIndex !== -1 
        if(isNotNewCategory){
            const categoryToUpdate = categoriesList[categoryIndex];
            const updatedCategory = { 
                ...categoryToUpdate,
                elements: categoryToUpdate.elements.concat(desiredItem),
            }
            categoriesList[categoryIndex] = updatedCategory;
            resultCategoryList = categoriesList;  
        } else {
            const newCategory = {...itemCategory, elements: [ desiredItem ] }
            resultCategoryList = categoriesList.concat(newCategory)
        }
        return resultCategoryList;
    }

    __getValuesOfElementsList(elements = []) {
        return this.dataConverter instanceof Function 
            ? elements.map( el => this.dataConverter(el))
            : elements;
    }

    __getDataSetByCallBack = getDataSet => {
        this.dataSet = getDataSet();
        return this.dataSet;
    }

    __getDataByApi =  async dataSourceApi => {
        if(__isDataSourceApiValid(dataSourceApi)) {
            const { url, method, arguments: args } = dataSourceApi;
            return await sendQuery(url, method, args);
        }
        throw 'Error getting data'
    }

    __isDataSourceApiValid = (dataSourceApi) => {
        const isUrlNotEmpty = !!dataSourceApi.url;
        const isMethodMotEmpty = !!dataSourceApi.method;
        return isUrlNotEmpty && isMethodMotEmpty;
    }

}